# PHP implementation for big integers 

Simple integer value object with big integer implementation.

Developed for a test, totally not production-grade.

Using this library, you can have really large integers represented as strings and:
- compare them
- add them toghether

Comparisons that can be performed:
- equal to
- not equal to
- greater or equal to
- less or equal to
- greater than
- less than

Adding two value objects together returns new value object as a result.

*Note: Only positive integers are allowed*

Comparison example:

```
$a = new BigInteger('12323543598732149872958714082798523523489723897423897423897429874987239847');
$b = new BigInteger('9999999999999999999999999999999999');

var_dump($a->greater_than($b)) (bool) true
var_dump($b->less_or_equal_than($a)) (bool) true

```


Addition example:

```
$a = new BigInteger('12323543598732149872958714082798523523489723897423897423897429874987239847);
$b = new BigInteger('1');

$c = $a->add($b);

echo $c->get_value(); // '12323543598732149872958714082798523523489723897423897423897429874987239848'
````

