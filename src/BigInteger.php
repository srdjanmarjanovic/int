<?php

namespace Int;

class BigInteger implements IntegerInterface
{
    /**
     * @var array
     */
    private $value;

    /**
     * BigIntegerInterface constructor.
     *
     * @param string $value
     */
    public function __construct($value)
    {
        $array_representation = str_split($value);

        if ($value == '-0' || $value == '+0') {
            $this->value = [0];
            return;
        } elseif (!preg_match('/^[0-9]*$/', $value) || is_null($value) || $value == ''){
            throw new \LogicException('Invalid representation of an integer. Only unsigned integers allowed');
        } elseif (array_sum($array_representation) === 0) {
            $this->value = [0];
            return;
        }

        $this->value = str_split(ltrim($value, 0));
    }

    /**
     * Return a string representation of the value.
     *
     * @return mixed
     */
    public function get_value()
    {
        return implode($this->value);
    }

    /**
     * Return true if this and $comparison_object values are equal.
     *
     * @param IntegerInterface $comparison_object
     * @return bool
     */
    public function equal_to(IntegerInterface $comparison_object): bool
    {
        $original_value = $this->get_value();
        $comparison_value = $comparison_object->get_value();

        $original_value_digits_count = strlen($original_value);
        $comparison_value_digits_count = strlen($comparison_value);

        if ($comparison_value_digits_count != $original_value_digits_count) {
            return false;
        } else {
            for($i = 0; $i <= $original_value_digits_count; $i++) {
                if (substr($original_value, $i, 1) != substr($comparison_value, $i, 1)) {
                    return false;
                }
            }

            return true;
        }
    }

    /**
     * Return true if this and $comparison_object values are not equal.
     *
     * @param IntegerInterface $comparison_object
     * @return bool
     */
    public function not_equal_to(IntegerInterface $comparison_object): bool
    {
        return !$this->equal_to($comparison_object);
    }

    /**
     * Return true if this value is larger than $comparison_object value.
     *
     * @param IntegerInterface $comparison_object
     * @return bool
     */
    public function greater_than(IntegerInterface $comparison_object): bool
    {
        return strnatcmp($this->get_value(), $comparison_object->get_value()) > 0;
    }

    /**
     * Return true if this value is smaller than $comparison_object value.
     *
     * @param IntegerInterface $comparison_object
     * @return bool
     */
    public function less_than(IntegerInterface $comparison_object): bool
    {
        return !$this->greater_than($comparison_object) && !$this->equal_to($comparison_object);
    }

    /**
     * Return true if this value is larger than or equal to $comparison_object value.
     *
     * @param IntegerInterface $comparison_object
     * @return bool
     */
    public function greater_or_equal_than(IntegerInterface $comparison_object): bool
    {
        return $this->greater_than($comparison_object) || $this->equal_to($comparison_object);
    }

    /**
     * Return true if this value is less than or equal to $comparison_object value.
     *
     * @param IntegerInterface $comparison_object
     * @return bool
     */
    public function less_or_equal_than(IntegerInterface $comparison_object): bool
    {
        return $this->less_than($comparison_object) || $this->equal_to($comparison_object);
    }

    /**
     * Returns a new instance of BigInteger with the two values added together.
     *
     * @param IntegerInterface $second_object
     * @return IntegerInterface
     */
    public function add(IntegerInterface $second_object): IntegerInterface
    {
        $original_value = $this->get_value();
        $second_value = $second_object->get_value();

        $original_value_digits_count = strlen($original_value);
        $second_value_digits_count = strlen($second_value);

        if ($second_value_digits_count > $original_value_digits_count) {
            $original_value = str_pad($original_value, $second_value_digits_count, '0', STR_PAD_LEFT);
        } elseif ($second_value_digits_count < $original_value_digits_count) {
            $second_value = str_pad($second_value, $original_value_digits_count, '0', STR_PAD_LEFT);
        }

        $reversed_second = strrev($second_value);
        $reversed_original = strrev($original_value);

        $reversed_sum_value = '';
        $carry = 0;

        for($i = 0; $i < strlen($second_value); $i++) {
            $temp_sum = (int) substr($reversed_original, $i, 1) + (int) substr($reversed_second, $i, 1) + $carry;

            if ($temp_sum > 9) {
                $temp = str_split($temp_sum);
                $carry = $temp[0];
                $temp_sum = $temp[1];
            } else {
                $carry = 0;
            }

            $reversed_sum_value .= (string) $temp_sum;
        }

        if ($carry) {
            $reversed_sum_value .= (string) $carry;
        }

        return new BigInteger(strrev($reversed_sum_value));
    }
}
