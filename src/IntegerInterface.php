<?php

namespace Int;

interface IntegerInterface
{
    /**
     * BigIntegerInterface constructor.
     *
     * @param string|array $value
     */
    public function __construct($value);

    /**
     * Return the value.
     *
     * @return mixed
     */
    public function get_value();

    /**
     * Return true if this and $comparison_object values are equal.
     *
     * @param IntegerInterface $comparison_object
     * @return bool
     */
    public function equal_to(IntegerInterface $comparison_object): bool;

    /**
     * Return true if this and $comparison_object values are not equal.
     *
     * @param IntegerInterface $comparison_object
     * @return bool
     */
    public function not_equal_to(IntegerInterface $comparison_object): bool;

    /**
     * Return true if this value is larger than $comparison_object value.
     *
     * @param IntegerInterface $comparison_object
     * @return bool
     */
    public function greater_than(IntegerInterface $comparison_object): bool;

    /**
     * Return true if this value is smaller than $comparison_object value.
     *
     * @param IntegerInterface $comparison_object
     * @return bool
     */
    public function less_than(IntegerInterface $comparison_object): bool;

    /**
     * Return true if this value is larger than or equal to $comparison_object value.
     *
     * @param IntegerInterface $comparison_object
     * @return bool
     */
    public function greater_or_equal_than(IntegerInterface $comparison_object): bool;

    /**
     * Return true if this value is less than or equal to $comparison_object value.
     *
     * @param IntegerInterface $comparison_object
     * @return bool
     */
    public function less_or_equal_than(IntegerInterface $comparison_object): bool;

    /**
     * Returns a new instance of BigInteger with the two values added together.
     *
     * @param IntegerInterface $second_object
     * @return IntegerInterface
     */
    public function add(IntegerInterface $second_object): IntegerInterface;
}