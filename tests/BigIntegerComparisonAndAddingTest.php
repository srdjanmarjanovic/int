<?php


namespace Int;

use PHPUnit\Framework\TestCase;

class BigIntegerComparisonAndAddingTest extends TestCase
{
    /**
     * @dataProvider equalityProvider
     * @param $a
     * @param $b
     * @param $expected
     */
    public function testEqualsTo($a, $b, $expected)
    {
        $big_int_a = new BigInteger($a);
        $big_int_b = new BigInteger($b);

        $this->assertEquals($expected, $big_int_a->equal_to($big_int_b));
        $this->assertEquals($expected, $big_int_b->equal_to($big_int_a));
    }

    /**
     * @dataProvider equalityProvider
     * @param $a
     * @param $b
     * @param $expected
     */
    public function testNotEqualsTo($a, $b, $expected)
    {
        $big_int_a = new BigInteger($a);
        $big_int_b = new BigInteger($b);

        $this->assertNotEquals($expected, $big_int_a->not_equal_to($big_int_b));
    }

    /**
     * @dataProvider integersForAdditionProvider
     *
     * @param $a
     * @param $b
     * @param $expected
     */
    public function testAddition($a, $b, $expected)
    {
        $big_int_a = new BigInteger($a);
        $big_int_b = new BigInteger($b);

        $big_int_c = $big_int_a->add($big_int_b);
        $big_int_d = $big_int_b->add($big_int_a);

        $this->assertEquals($expected, $big_int_c->get_value());
        $this->assertEquals($expected, $big_int_d->get_value());
    }


    public function testGreaterThan()
    {
        $big_int_a = new BigInteger('0');
        $big_int_b = new BigInteger('1');
        $big_int_c = new BigInteger('1000');
        $big_int_d = new BigInteger('12323543598732149872958714082798523523489723897423897423897429874987239847');
        $big_int_e = new BigInteger('9999999999999999999999999999999999');

        $this->assertFalse($big_int_a->greater_than($big_int_b));
        $this->assertFalse($big_int_a->greater_than($big_int_c));
        $this->assertFalse($big_int_a->greater_than($big_int_d));
        $this->assertFalse($big_int_a->greater_than($big_int_e));

        $this->assertTrue($big_int_b->greater_than($big_int_a));
        $this->assertFalse($big_int_b->greater_than($big_int_c));
        $this->assertFalse($big_int_b->greater_than($big_int_d));
        $this->assertFalse($big_int_b->greater_than($big_int_e));

        $this->assertTrue($big_int_c->greater_than($big_int_a));
        $this->assertTrue($big_int_c->greater_than($big_int_b));
        $this->assertFalse($big_int_c->greater_than($big_int_d));
        $this->assertFalse($big_int_c->greater_than($big_int_e));

        $this->assertTrue($big_int_d->greater_than($big_int_a));
        $this->assertTrue($big_int_d->greater_than($big_int_b));
        $this->assertTrue($big_int_d->greater_than($big_int_c));
        $this->assertTrue($big_int_d->greater_than($big_int_e));

        $this->assertTrue($big_int_e->greater_than($big_int_a));
        $this->assertTrue($big_int_e->greater_than($big_int_b));
        $this->assertTrue($big_int_e->greater_than($big_int_c));
        $this->assertFalse($big_int_e->greater_than($big_int_d));
    }

    public function testLessThan()
    {
        $big_int_a = new BigInteger('0');
        $big_int_b = new BigInteger('1');
        $big_int_b1 = new BigInteger('01');
        $big_int_c = new BigInteger('1000');
        $big_int_d = new BigInteger('12323543598732149872958714082798523523489723897423897423897429874987239847');
        $big_int_e = new BigInteger('9999999999999999999999999999999999');

        $this->assertFalse($big_int_b1->less_than($big_int_b));
        $this->assertFalse($big_int_b->less_than($big_int_b1));

        $this->assertTrue($big_int_a->less_than($big_int_b));
        $this->assertTrue($big_int_a->less_than($big_int_c));
        $this->assertTrue($big_int_a->less_than($big_int_d));
        $this->assertTrue($big_int_a->less_than($big_int_e));

        $this->assertFalse($big_int_b->less_than($big_int_a));
        $this->assertTrue($big_int_b->less_than($big_int_c));
        $this->assertTrue($big_int_b->less_than($big_int_d));
        $this->assertTrue($big_int_b->less_than($big_int_e));

        $this->assertFalse($big_int_c->less_than($big_int_a));
        $this->assertFalse($big_int_c->less_than($big_int_b));
        $this->assertTrue($big_int_c->less_than($big_int_d));
        $this->assertTrue($big_int_c->less_than($big_int_e));

        $this->assertFalse($big_int_d->less_than($big_int_a));
        $this->assertFalse($big_int_d->less_than($big_int_b));
        $this->assertFalse($big_int_d->less_than($big_int_c));
        $this->assertFalse($big_int_d->less_than($big_int_e));

        $this->assertFalse($big_int_e->less_than($big_int_a));
        $this->assertFalse($big_int_e->less_than($big_int_b));
        $this->assertFalse($big_int_e->less_than($big_int_c));
        $this->assertTrue($big_int_e->less_than($big_int_d));
    }

    public function testGreaterOrEqualThan()
    {
        $big_int_a = new BigInteger('0');
        $big_int_b = new BigInteger('1');
        $big_int_b1 = new BigInteger('01');
        $big_int_c = new BigInteger('10');
        $big_int_d = new BigInteger('12323543598732149872958714082798523523489723897423897423897429874987239847');
        $big_int_e = new BigInteger('9999999999999999999999999999999999');

        $this->assertFalse($big_int_a->greater_or_equal_than($big_int_b));
        $this->assertFalse($big_int_a->greater_or_equal_than($big_int_b1));
        $this->assertFalse($big_int_a->greater_or_equal_than($big_int_c));
        $this->assertFalse($big_int_a->greater_or_equal_than($big_int_d));
        $this->assertFalse($big_int_a->greater_or_equal_than($big_int_e));

        $this->assertTrue($big_int_b->greater_or_equal_than($big_int_a));
        $this->assertTrue($big_int_b->greater_or_equal_than($big_int_b1));
        $this->assertFalse($big_int_b->greater_or_equal_than($big_int_c));
        $this->assertFalse($big_int_b->greater_or_equal_than($big_int_d));
        $this->assertFalse($big_int_b->greater_or_equal_than($big_int_e));

        $this->assertTrue($big_int_b1->greater_or_equal_than($big_int_a));
        $this->assertTrue($big_int_b1->greater_or_equal_than($big_int_b));
        $this->assertFalse($big_int_b1->greater_or_equal_than($big_int_c));
        $this->assertFalse($big_int_b1->greater_or_equal_than($big_int_d));
        $this->assertFalse($big_int_b1->greater_or_equal_than($big_int_e));

        $this->assertTrue($big_int_c->greater_or_equal_than($big_int_a));
        $this->assertTrue($big_int_c->greater_or_equal_than($big_int_b));
        $this->assertTrue($big_int_c->greater_or_equal_than($big_int_b1));
        $this->assertFalse($big_int_c->greater_or_equal_than($big_int_d));
        $this->assertFalse($big_int_c->greater_or_equal_than($big_int_e));

        $this->assertTrue($big_int_d->greater_or_equal_than($big_int_a));
        $this->assertTrue($big_int_d->greater_or_equal_than($big_int_b));
        $this->assertTrue($big_int_d->greater_or_equal_than($big_int_b1));
        $this->assertTrue($big_int_d->greater_or_equal_than($big_int_c));
        $this->assertTrue($big_int_d->greater_or_equal_than($big_int_e));

        $this->assertTrue($big_int_e->greater_or_equal_than($big_int_a));
        $this->assertTrue($big_int_e->greater_or_equal_than($big_int_b));
        $this->assertTrue($big_int_e->greater_or_equal_than($big_int_b1));
        $this->assertTrue($big_int_e->greater_or_equal_than($big_int_c));
        $this->assertFalse($big_int_e->greater_or_equal_than($big_int_d));
    }

    public function testLessOrEqualThan()
    {
        $big_int_a = new BigInteger('0');
        $big_int_b = new BigInteger('1');
        $big_int_b1 = new BigInteger('01');
        $big_int_c = new BigInteger('10');
        $big_int_d = new BigInteger('12323543598732149872958714082798523523489723897423897423897429874987239847');
        $big_int_e = new BigInteger('9999999999999999999999999999999999');

        $this->assertTrue($big_int_a->less_or_equal_than($big_int_b));
        $this->assertTrue($big_int_a->less_or_equal_than($big_int_b1));
        $this->assertTrue($big_int_a->less_or_equal_than($big_int_c));
        $this->assertTrue($big_int_a->less_or_equal_than($big_int_d));
        $this->assertTrue($big_int_a->less_or_equal_than($big_int_e));

        $this->assertFalse($big_int_b->less_or_equal_than($big_int_a));
        $this->assertTrue($big_int_b->less_or_equal_than($big_int_b1));
        $this->assertTrue($big_int_b->less_or_equal_than($big_int_c));
        $this->assertTrue($big_int_b->less_or_equal_than($big_int_d));
        $this->assertTrue($big_int_b->less_or_equal_than($big_int_e));

        $this->assertFalse($big_int_b1->less_or_equal_than($big_int_a));
        $this->assertTrue($big_int_b1->less_or_equal_than($big_int_b));
        $this->assertTrue($big_int_b1->less_or_equal_than($big_int_c));
        $this->assertTrue($big_int_b1->less_or_equal_than($big_int_d));
        $this->assertTrue($big_int_b1->less_or_equal_than($big_int_e));

        $this->assertFalse($big_int_c->less_or_equal_than($big_int_a));
        $this->assertFalse($big_int_c->less_or_equal_than($big_int_b));
        $this->assertFalse($big_int_c->less_or_equal_than($big_int_b1));
        $this->assertTrue($big_int_c->less_or_equal_than($big_int_d));
        $this->assertTrue($big_int_c->less_or_equal_than($big_int_e));

        $this->assertFalse($big_int_d->less_or_equal_than($big_int_a));
        $this->assertFalse($big_int_d->less_or_equal_than($big_int_b));
        $this->assertFalse($big_int_d->less_or_equal_than($big_int_b1));
        $this->assertFalse($big_int_d->less_or_equal_than($big_int_c));
        $this->assertFalse($big_int_d->less_or_equal_than($big_int_e));

        $this->assertFalse($big_int_e->less_or_equal_than($big_int_a));
        $this->assertFalse($big_int_e->less_or_equal_than($big_int_b));
        $this->assertFalse($big_int_e->less_or_equal_than($big_int_b1));
        $this->assertFalse($big_int_e->less_or_equal_than($big_int_c));
        $this->assertTrue($big_int_e->less_or_equal_than($big_int_d));
    }

    /**
     * @return array
     */
    public function equalityProvider()
    {
        return [
            ['0', '0', true],
            ['00', '0', true],
            ['1', '1', true],
            ['01', '1', true],
            ['12323543598732149872958714082798523523489723897423897423897429874987239847', '1', false],
            ['12323543598732149872958714082798523523489723897423897423897429874987239847', '12323543598732149872958714082798523523489723897423897423897429874987239847', true],
        ];
    }

    /**
     * @return array
     */
    public function integersForAdditionProvider()
    {
        return [
            ['1', '2', '3'],
            ['2712', '360', '3072'],
            ['999', '1', '1000'],
            ['999', '223', '1222'],
            ['12323543598732149872958714082798523523489723897423897423897429874987239847', '1', '12323543598732149872958714082798523523489723897423897423897429874987239848'],
        ];
    }
}