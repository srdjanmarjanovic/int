<?php

namespace Int;

use PHPUnit\Framework\TestCase;

class BigIntegerInstantiationAndPreparationTest extends TestCase
{
    public function testZeroCanBeValue()
    {
        $big_int = new BigInteger('0');
        $this->assertEquals('0', $big_int->get_value());
    }

    public function testSignedAndUnsignedZerosAreTreatedAsZero()
    {
        $negative_zero = new BigInteger('-0');
        $unsigned_positive_zero = new BigInteger('0');
        $signed_positive_zero = new BigInteger('+0');

        $this->assertEquals('0', $negative_zero->get_value());
        $this->assertEquals('0', $unsigned_positive_zero->get_value());
        $this->assertEquals('0', $signed_positive_zero->get_value());
    }

    /**
     * @dataProvider invalidRepresentationsProvider
     *
     * @expectedException \LogicException
     * @expectedExceptionMessage Invalid representation of an integer. Only unsigned integers allowed
     */
    public function testInvalidRepresentationOfAnIntegerThrowsAndException($invalid_representation)
    {
        new BigInteger($invalid_representation);
    }

    /**
     * @dataProvider trimmingsProvider
     * @param $given
     * @param $expected
     */
    public function testTrimming($given, $expected)
    {
        $int = new BigInteger($given);
        $this->assertSame($expected, $int->get_value());
    }

    /**
     * @return array
     */
    public function invalidRepresentationsProvider()
    {
        return [
            [''],
            ['a'],
            [' '],
            ['-1'],
            [' 0123'],
            [' 027'],
            ['0 123'],
            ['0123 '],
        ];
    }

    /**
     * @return array
     */
    public function trimmingsProvider()
    {
        return [
            ['0', '0'],
            ['01', '1'],
            ['000', '0'],
            ['1', '1'],
            ['10', '10'],
            ['010', '10'],
            ['12323543598732149872958714082798523523489723897423897423897429874987239847', '12323543598732149872958714082798523523489723897423897423897429874987239847']
        ];
    }
}